import * as multer from "multer";
import * as path from 'path';

const storage = multer.diskStorage({
  destination: function (request, file, cb) {
    cb(null, "public/images/");
  },
  filename: function (request, file, cb) {
    cb(null, file.originalname);
  }
});

const imageFilter = function (request, file, cb) {
  const filetypes = /jpeg|jpg|png/;
  const mimetype = filetypes.test(file.mimetype);
  const extname = filetypes.test(path.extname(file.originalname).toLowerCase());

  if (mimetype && extname) {
    return cb(null, true);
  }
  cb(new Error(`File upload only supports the following filetypes - ${filetypes}`));
};

const limits = { fileSize: 1024 * 1024 }

const uploads = multer({ storage: storage, fileFilter: imageFilter, limits: limits });

export default uploads;