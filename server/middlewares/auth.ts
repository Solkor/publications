import * as jwt from 'jsonwebtoken';
import * as config from './../../config/config';
import { promisify } from 'util';

export const checkAuth = async (request, response, next) => {
    var token = request.headers['authorization'];
    if (!token) {
        return response.status(403).send('No token provided');
    }

    const verify = promisify(jwt.verify);
    try {
        const decode = await verify(token, config.jwt['secret']);
        request.user = {
            id: decode['id'],
            name: decode['name']
        }
        next();
    } catch (error) {
        return response.sendStatus(401);
    }
};
