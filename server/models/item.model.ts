module.exports = function(sequelize, DataTypes) {
    const Items = sequelize.define(
        'items',
        {
            id: {
                type: DataTypes.INTEGER(11).UNSIGNED,
                autoIncrement: true,
                primaryKey: true,
                field: 'id'
            },
            image: {
                type: DataTypes.CHAR(100),
                get: function() {
                    if (this.getDataValue('image')) {
                        return `http://localhost:3000/images/${this.getDataValue('image')}`;
                    }
                    return this.getDataValue('image');
                }
            },
            created_at: {
                type: DataTypes.DATE,
                field: 'created_at',
                defaultValue: sequelize.literal('NOW()'),
                get: function() {
                    return Date.parse(this.getDataValue('created_at')) / 1000;
                }
            },
            title: DataTypes.CHAR(100),
            description: DataTypes.STRING,
            price: DataTypes.DECIMAL(6,2),
            user_id: DataTypes.INTEGER(11).UNSIGNED
        },
        {
            timestamps: true,
            createdAt: 'created_at',
            updatedAt: false,
            freezeTableName: true
        }
    );

    Items.associate = function(models) {
        Items.belongsTo(models.users, { targetKey: 'id', foreignKey: 'user_id' });
    };

    return Items;
};
