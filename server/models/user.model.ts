import * as bcrypt from 'bcrypt';

module.exports = function(sequelize, DataTypes) {
    const Users = sequelize.define(
        'users',
        {
            id: {
                type: DataTypes.INTEGER(11).UNSIGNED,
                autoIncrement: true,
                primaryKey: true,
                field: 'id'
            },
            name: DataTypes.CHAR(50),
            email: DataTypes.CHAR(50),
            password: DataTypes.CHAR(50),
            phone: DataTypes.CHAR(50)
        },
        {
            hooks: {
                beforeCreate: async user => {
                    let salt = await bcrypt.genSalt(12);
                    user.password = await bcrypt.hash(user.password, salt);
                },
                beforeUpdate: async user => {
                    if (!user.changed('password')) return null;
                    let salt = await bcrypt.genSalt(12);
                    user.password = await bcrypt.hash(user.password, salt);
                }
            },
            timestamps: false,
            freezeTableName: true
        }
    );

    return Users;
};
