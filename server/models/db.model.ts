import * as fs from 'fs';
import * as path from 'path';
import * as Sequelize from 'sequelize';
import * as config from './../../config/config';

const basename = path.basename(__filename);
const sequelize = new Sequelize(config.dbConfig['database'], config.dbConfig['username'], config.dbConfig['password'], {
    dialectOptions: { socketPath: config.dbConfig['socketPath'] },
    host: config.dbConfig['host'],
    port: config.dbConfig['host'],
    dialect: 'mysql',
    timezone: '+03:00',
    operatorsAliases: false,
    logging: console.log
});
// const sequelize = new Sequelize('mysql://root@127.0.0.1:3306/publications', {timezone: '+03:00'});
export let db: any = {};

fs
    .readdirSync(__dirname)
    .filter(file => {
        return file.indexOf('.') !== 0 && file !== basename && file.slice(-3) === '.js';
    })
    .forEach(file => {
        const model = sequelize['import'](path.join(__dirname, file));
        db[model.name] = model;
    });

Object.keys(db).forEach(modelName => {
    if (db[modelName].associate) {
        db[modelName].associate(db);
    }
});

sequelize
    .authenticate()
    .then(() => {
        console.log('Connection to database has been established successfully.');
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });

db.sequelize = sequelize;
db.Sequelize = Sequelize;
