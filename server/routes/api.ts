import { Router, Request, Response, NextFunction } from 'express';
import AuthController from '../controllers/auth.controller';
import UserController from '../controllers/user.controller';
import ItemController from '../controllers/item.controller';
import { checkAuth } from '../middlewares/auth';
import { db } from './../models/db.model';

class ApiRouter {
    public router: Router;

    constructor() {
        this.router = Router();
        this.setRouter();
    }

    private setRouter(): void {
        this.router.param('id', async (request: Request, response: Response, next: NextFunction, id) => {
            try {
                const item = await ItemController.checkItem(id);
                if (!item) {
                    return response.sendStatus(404);
                }
                request['item'] = item;
                next();
            } catch (error) {
                response.sendStatus(500);
            }
        });
        //Login user
        this.router.post('/login', async (request: Request, response: Response) => {
            try {
                await AuthController.logIn(request, response);
            } catch (error) {
                response.sendStatus(500);
            }
        });
        //Register
        this.router.post('/register', async (request: Request, response: Response) => {
            try {
                await AuthController.signUp(request, response);
            } catch (error) {
                console.log(error);
                response.sendStatus(500);
            }
        });
        //Me
        this.router
            .route('/me')
            //Get my information
            .get(checkAuth, async (request: Request, response: Response) => {
                try {
                    await UserController.getUser(request, response);
                } catch (error) {
                    response.sendStatus(500);
                }
            })
            //Update my information
            .put(checkAuth, async (request: Request, response: Response) => {
                try {
                    await UserController.updateUser(request, response);
                } catch (error) {
                    console.log(error);
                    response.sendStatus(500);
                }
            });
        //Get user by ID
        this.router.get('/user/:userId(\\d+)/', checkAuth, async (request: Request, response: Response) => {
            try {
                await UserController.getUserById(request, response);
            } catch (error) {
                response.sendStatus(500);
            }
        });
        //Search users
        this.router.get('/user', async (request: Request, response: Response) => {
            try {
                await UserController.searchUser(request, response);
            } catch (error) {
                response.sendStatus(500);
            }
        });
        //Item
        this.router
            .route('/item/:id(\\d+)/')
            //Get item by ID
            .get(checkAuth, async (request: Request, response: Response) => {
                try {
                    await ItemController.getItemById(request, response);
                } catch (error) {
                    response.sendStatus(500);
                }
            })
            //Update item
            .put(checkAuth, async (request: Request, response: Response) => {
                try {
                    await ItemController.updateItem(request, response);
                } catch (error) {
                    response.sendStatus(500);
                }
            })
            //Delete item
            .delete(checkAuth, async (request: Request, response: Response) => {
                try {
                    await ItemController.deleteItem(request, response);
                } catch (error) {
                    response.sendStatus(500);
                }
            });
        //Create item
        this.router.post('/item', checkAuth, async (request: Request, response: Response) => {
            try {
                await ItemController.createItem(request, response);
            } catch (error) {
                response.sendStatus(500);
            }
        });
        //Search items
        this.router.get('/item', async (request: Request, response: Response) => {
            try {
                await ItemController.searchItems(request, response);
            } catch (error) {
                response.sendStatus(500);
            }
        });
        //Upload item image
        this.router
            .route('/item/:id/image')
            .post(checkAuth, async (request: Request, response: Response) => {
                try {
                    await ItemController.saveImage(request, response);
                } catch (error) {
                    response.sendStatus(500);
                }
            })
            .delete(checkAuth, async (request: Request, response: Response) => {
                try {
                    await ItemController.deleteImage(request, response);
                } catch (error) {
                    response.sendStatus(500);
                }
            });
    }
}

export default new ApiRouter().router;
