import * as bcrypt from 'bcrypt';

export class Validators {
    private field: string;
    private value: string;
    private result = {
        status: true,
        errors: {}
    };

    constructor(field: string, value: string) {
        this.field = field;
        this.value = value;
    }

    isEmpty(message?: string): Validators {
        if (this.value.length === 0) {
            Object.assign(this.result.errors, {
                field: this.field,
                message: message ? message : "Value can't be empty"
            });
            this.result.status = false;
        }
        return this;
    }

    toLong(number: number, message?: string): Validators {
        if (this.value.length >= number) {
            Object.assign(this.result.errors, {
                field: this.field,
                message: message ? message : `Value must be less than ${number} chars`
            });
            this.result.status = false;
        }
        return this;
    }

    toShort(number: number, message?: string): Validators {
        if (this.value.length <= number) {
            Object.assign(this.result.errors, {
                field: this.field,
                message: message ? message : `Value must be greater than ${number} chars`
            });
            this.result.status = false;
        }
        return this;
    }

    isMax(number: number, message?: string): Validators {
        if (parseInt(this.value) > number) {
            Object.assign(this.result.errors, {
                field: this.field,
                message: message ? message : `Value must be less than ${number}`
            });
            this.result.status = false;
        }
        return this;
    }

    matches(pattern: RegExp, message?: string): Validators {
        if (!pattern.test(this.value)) {
            Object.assign(this.result.errors, { field: this.field, message: message ? message : 'Invalid value' });
            this.result.status = false;
        }
        return this;
    }

    async isExist(db: any, where: object, message?: string): Promise<Validators> {
        await db.findAndCountAll(where).then(result => {
            if (result.count) {
                Object.assign(this.result.errors, {
                    field: this.field,
                    message: message ? message : 'Value already exist'
                });
                this.result.status = false;
            }
        });

        return this;
    }

    async isNoExist(db: any, where: object, message?: string): Promise<Validators> {
        await db.findAndCountAll(where).then(result => {
            if (!result.count) {
                Object.assign(this.result.errors, {
                    field: this.field,
                    message: message ? message : 'Value no exist'
                });
                this.result.status = false;
            }
        });

        return this;
    }

    isMobilePhone(message?: string): Validators {
        const phones = {
            UA: /^(\+?38|8)?0\d{9}$/
        };

        if (!phones.UA.test(this.value)) {
            Object.assign(this.result.errors, {
                field: this.field,
                message: message ? message : 'Invalid phone number'
            });
            this.result.status = false;
        }
        return this;
    }
    async checkPassword(password: string, user: object, db: any, message?: string): Promise<Validators> {
        const userData = await db.findOne({ where: { id: user['id'] }, raw: true });
        const compare = await bcrypt.compare(password, userData.password);

        if (!compare) {
            Object.assign(this.result.errors, { field: this.field, message: message ? message : 'Invalid password' });
            this.result.status = false;
        }

        return this;
    }

    validate(): object {
        return this.result;
    }
}
