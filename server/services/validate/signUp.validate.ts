import { Validators } from '../../services/validate/validators';
import { Request } from 'express';

class SignUpValidate {
    private methods = {
        name: { method: this['validateName'], required: true },
        password: { method: this['validatePassword'], required: true },
        email: { method: this['validateEmail'], required: true },
        phone: { method: this['validatePhone'], required: false }
    };

    public async validate(request: Request) {
        const result = {
            errors: [],
            status: true
        };

        await Promise.all(
            Object.keys(this.methods).map(async method => {
                if (
                    request.body[method] ||
                    request.body[method] === '' ||
                    (!request.body[method] && this.methods[method].required)
                ) {
                    const validationStatus = await this.methods[method].method(
                        method,
                        request.body[method] ? request.body[method] : ''
                    );
                    if (Object.keys(validationStatus.errors).length) {
                        result.errors.push(validationStatus.errors);
                    }
                    result.status = result.status && validationStatus.status;
                }
            })
        );

        return result;
    }

    private async validateName(field: string, value: string): Promise<any> {
        return new Promise((resolve, reject) => {
            const name = new Validators(field, value).isEmpty('Name can not be empty').validate();

            resolve(name);
        });
    }

    private async validatePassword(field: string, value: string): Promise<any> {
        return new Promise((resolve, reject) => {
            const pwd = new Validators(field, value).isEmpty('Password can not be empty').validate();

            resolve(pwd);
        });
    }

    private async validateEmail(field: string, value: string): Promise<any> {
        return new Promise((resolve, reject) => {
            const email = new Validators(field, value)
                .isEmpty('Email can not be empty')
                .matches(
                    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                    'Invalid email address'
                )
                .validate();

            resolve(email);
        });
    }

    private async validatePhone(field: string, value: string): Promise<any> {
        return new Promise((resolve, reject) => {
            const phone = new Validators(field, value)
                .isMobilePhone('Invalid phone number')
                .isEmpty('Phone number can not be empty')
                .validate();

            resolve(phone);
        });
    }
}

export default new SignUpValidate();
