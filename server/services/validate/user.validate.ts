import { Validators } from '../../services/validate/validators';
import { Op } from 'sequelize';
import { Request } from 'express';
import { db } from '../../models/db.model';

class UserValidate {
    private methods = {
        name: { method: this['validateName'], required: false },
        email: { method: this['validateEmail'], required: false },
        phone: { method: this['validatePhone'], required: false },
        current_password: {
            method: this['validateCurrentPassword'],
            required: false,
            dependency: { new_password: { method: this['validateNewPassword'], required: true } }
        }
    };

    public async validate(request: Request) {
        const result = {
            saveData: {},
            errors: [],
            status: true
        };

        const recursiveValidate = async (methods, request: Request) => {
            await Promise.all(
                Object.keys(methods).map(async method => {
                    if (
                        request.body[method] ||
                        request.body[method] === '' ||
                        (!request.body[method] && methods[method].required)
                    ) {
                        const validationStatus = await methods[method].method(
                            method,
                            request.body[method] ? request.body[method] : '',
                            request
                        );
                        result.saveData[method] = request.body[method];
                        if (Object.keys(validationStatus.errors).length) {
                            result.errors.push(validationStatus.errors);
                        }
                        result.status = result.status && validationStatus.status;
                        if (methods[method].dependency) {
                            recursiveValidate(methods[method].dependency, request);
                        }
                    }
                })
            );
        };
        await recursiveValidate(this.methods, request);
        return result;
    }

    private async validateName(field: string, value: string, request: Request): Promise<any> {
        return new Promise((resolve, reject) => {
            const name = new Validators(field, value).isEmpty('Name can not be empty').toLong(30, 'Name must be less than 30 chars' ).validate();

            resolve(name);
        });
    }

    private async validateEmail(field: string, value: string, request: Request): Promise<any> {
        return new Promise((resolve, reject) => {
            const email = new Validators(field, value)
                .isEmpty('Email is required')
                .matches(
                    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                    'Invalid email address'
                )
                .isExist(
                    db.users,
                    {
                        where: { email: value, id: { [Op.ne]: request['user'].id } }
                    },
                    'Email already exist'
                )
                .then(result => result.validate());

            resolve(email);
        });
    }

    private async validatePhone(field: string, value: string, request: Request): Promise<any> {
        return new Promise((resolve, reject) => {
            const phone = new Validators(field, value)
                .isMobilePhone('Invalid phone number')
                .isEmpty('Phone number can not be empty')
                .validate();

            resolve(phone);
        });
    }

    private async validateCurrentPassword(field: string, value: string, request: Request): Promise<any> {
        return new Promise((resolve, reject) => {
            const currentPassword = new Validators(field, value)
                .checkPassword(value, request['user'], db.users, 'Wrong current password')
                .then(result => result.validate());
            resolve(currentPassword);
        });
    }

    private async validateNewPassword(field: string, value: string, request: Request): Promise<any> {
        return new Promise((resolve, reject) => {
            const newPassword = new Validators(field, value).isEmpty('New password can not be empty').validate();
            resolve(newPassword);
        });
    }
}

export default new UserValidate();
