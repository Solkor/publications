import { Validators } from '../../services/validate/validators';
import { Request } from 'express';
import { db } from '../../models/db.model';

class ItemValidate {
    private methods = {
        title: { method: this['validateTitle'], required: true },
        description: { method: this['validateDescription'], required: true },
        price: { method: this['validatePrice'], required: true }
    };

    public async validate(request: Request) {
        const result = {
            saveData: {},
            errors: [],
            status: true
        };

        const recursiveValidate = async (methods, request: Request) => {
            await Promise.all(
                Object.keys(methods).map(async method => {
                    if (
                        request.body[method] ||
                        request.body[method] === '' ||
                        (!request.body[method] && methods[method].required)
                    ) {
                        const validationStatus = await methods[method].method(
                            method,
                            request.body[method] ? request.body[method] : '',
                            request
                        );
                        result.saveData[method] = request.body[method];
                        if (Object.keys(validationStatus.errors).length) {
                            result.errors.push(validationStatus.errors);
                        }
                        result.status = result.status && validationStatus.status;
                        if (methods[method].dependency) {
                            recursiveValidate(methods[method].dependency, request);
                        }
                    }
                })
            );
        };
        await recursiveValidate(this.methods, request);
        return result;
    }

    private async validateTitle(field: string, value: string, request: Request): Promise<any> {
        return new Promise((resolve, reject) => {
            const title = new Validators(field, value)
                .isEmpty('Title can not be empty')
                .toShort(5, 'Title must be greater than 5 chars')
                .toLong(100, 'Name must be less than 100 chars')
                .validate();
            resolve(title);
        });
    }

    private async validateDescription(field: string, value: string, request: Request): Promise<any> {
        return new Promise((resolve, reject) => {
            const description = new Validators(field, value)
                .isEmpty('Description can not be empty')
                .toShort(5, 'Description must be greater than 5 chars')
                .toLong(250, 'Name must be less than 250 chars')
                .validate();
            resolve(description);
        });
    }

    private async validatePrice(field: string, value: string, request: Request): Promise<any> {
        return new Promise((resolve, reject) => {
            const description = new Validators(field, value)
                .isEmpty('Price can not be empty')
                .isMax(9999, 'Price must be lees than 9999')
                .matches(/^\d{1,5}(\.\d{1,2})?$/gm, 'Invalid price')
                .validate();
            resolve(description);
        });
    }
}

export default new ItemValidate();
