import { db } from './../models/db.model';
import * as jwt from 'jsonwebtoken';
import * as config from './../../config/config';
import * as bcrypt from 'bcrypt';
import signUpValidate from '../services/validate/signUp.validate';

class AuthController {
    constructor() {}

    public async signUp(request, response) {
        const validationResult = await signUpValidate.validate(request);
        if (!validationResult.status) {
            return response.status(422).json(validationResult.errors);
        }

        const user = await db.users.create(request.body, { isNewRecord: true });
        const token = jwt.sign(user.dataValues, config.jwt['secret'], {
            expiresIn: config.jwt['expireTime']
        });
        response.status(200).json({ token: token });
    }

    public async logIn(request, response) {
        const user = await db.users.findOne({ where: { email: request.body.email }, raw: true });
        if (!user) {
            return response.status(422).json({ field: 'email', message: 'Wrong email or password' });
        }
        const compare = await bcrypt.compare(request.body.password, user.password);
        if (!compare) {
            return response.status(422).json({ field: 'password', message: 'Wrong email or password' });
        }
        const token = jwt.sign(user, config.jwt['secret'], {
            expiresIn: config.jwt['expireTime']
        });
        response.status(200).json({ token: token });
    }
}

export default new AuthController();
