import { db } from './../models/db.model';
import userValidate from '../services/validate/user.validate';

class UserController {
    constructor() {}

    public async getUser(request, response) {
        const user = await db.users.findOne({
            where: { id: request.user.id },
            attributes: ['id', 'phone', 'name', 'email']
        });
        response.status(200).json(user.dataValues);
    }

    public async updateUser(request, response) {
        const updateData = {};
        const validationResult = await userValidate.validate(request);

        if (!validationResult.status) {
            return response.status(422).json(validationResult);
        }

        Object.keys(validationResult.saveData).map(field => {
            if (field === 'new_password') {
                updateData['password'] = validationResult.saveData['new_password'];
            } else {
                updateData[field] = validationResult.saveData[field];
            }
        });

        await db.users.update(updateData, { where: { id: request.user.id }, individualHooks: true });
        const user = await db.users.findOne({
            where: { id: request.user.id },
            attributes: ['id', 'phone', 'name', 'email']
        });
        response.status(200).json(user.dataValues);
    }

    public async getUserById(request, response) {
        const user = await db.users.findOne({
            where: { id: request.params.userId },
            attributes: ['id', 'phone', 'name', 'email']
        });
        if (!user) {
            return response.sendStatus(404);
        }
        response.status(200).json(user.dataValues);
    }

    public async searchUser(request, response) {
        const searchData = {};
        Object.keys(request.query).map(async field => {
            if (field in db.users.attributes && (field === 'name' || field === 'email')) {
                searchData[field] = request.query[field];
            }
        });

        const user = await db.users.findAll({
            where: searchData,
            attributes: ['id', 'phone', 'name', 'email']
        });
        response.status(200).json(user);
    }
}

export default new UserController();
