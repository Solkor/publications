import { db } from './../models/db.model';
import uploads from './../middlewares/uploads';
import * as fs from 'fs';
import { promisify } from 'util';
import itemValidate from '../services/validate/item.validate';

class ItemController {
    constructor() {}

    public async checkItem(id: number) {
        return await db.items.findOne({ where: { id: id }, raw: true });
    }

    public async searchItems(request, response) {
        const fields = {
            title: { type: 'where' },
            user_id: { type: 'where' },
            order_by: { type: 'order_by', values: ['price', 'created_at'], default: 'created_at' },
            order_type: { type: 'order_type', values: ['asc', 'desc'], default: 'desc' }
        };

        const searchData = {};

        Object.keys(fields).map(async field => {
            if ( request.query[field] || fields[field].default ) {
                const obj = {};
                if(fields[field].values && (fields[field].values.indexOf(request.query[field]) === -1)) {
                    obj[field] = fields[field].default;
                } else {
                    obj[field] = request.query[field];
                }

                searchData[fields[field].type] ? Object.assign(searchData[fields[field].type], obj) : searchData[fields[field].type] = obj;
            }
        });

        const items = await db.items.findAll({
            include: [
                {
                    model: db.users,
                    attributes: ['id', 'phone', 'name', 'email']
                }
            ],
            where: searchData['where'],
            order: [[searchData['order_by'].order_by, searchData['order_type'].order_type]]
        });

        response.json(items);
    }

    public async createItem(request, response) {
        const validationResult = await itemValidate.validate(request);

        if (!validationResult.status) {
            return response.status(422).json(validationResult.errors);
        }

        Object.assign(validationResult.saveData, { user_id: request.user.id });
        const createItem = await db.items.create(validationResult.saveData);
        const item = await db.items.findOne({
            include: [
                {
                    model: db.users,
                    attributes: ['id', 'phone', 'name', 'email']
                }
            ],
            where: { id: createItem.dataValues.id }
        });
        response.status(200).json(item);
    }

    public async updateItem(request, response) {
        const validationResult = await itemValidate.validate(request);

        if (!validationResult.status) {
            return response.status(422).json(validationResult.errors);
        }

        await db.items.update(validationResult.saveData, { where: { id: request.params.id } });
        const item = await db.items.findAll({
            include: [
                {
                    model: db.users,
                    attributes: ['id', 'phone', 'name', 'email']
                }
            ],
            where: { id: request.params.id }
        });

        response.status(200).json(item);
    }

    public async deleteItem(request, response) {
        const exists = promisify(fs.exists);
        const unlink = promisify(fs.unlink);
        if (request.item.image) {
            const path = `public/images/${request.item.image}`;
            const fileExist = await exists(path);
            if (fileExist) {
                await unlink(path);
            }
        }
        await db.items.destroy({ where: { id: request.params.id } });
        response.sendStatus(200);
    }

    public async getItemById(request, response) {
        const item = await db.items.findAll({
            include: [
                {
                    model: db.users,
                    attributes: ['id', 'phone', 'name', 'email']
                }
            ],
            where: { id: request.params.id }
        });

        response.status(200).json(item);
    }

    public async saveImage(request, response) {
        const upload = new Promise(resolve => {
            uploads.single('file')(request, response, function(error) {
                if (error) {
                    return response.status(422).json({ field: 'file', message: error.message });
                }
                resolve(request.file);
            });
        });

        const exists = promisify(fs.exists);
        const unlink = promisify(fs.unlink);
        if (request.item.image) {
            const path = `public/images/${request.item.image}`;
            const fileExist = await exists(path);
            if (fileExist) {
                await unlink(path);
            }
        }
        const file = await upload;
        await db.items.update({ image: file['filename'] }, { where: { id: request.item.id } });
        const item = await db.items.findAll({
            include: [
                {
                    model: db.users,
                    attributes: ['id', 'phone', 'name', 'email']
                }
            ],
            where: { id: request.params.id }
        });
        response.status(200).json(item);
    }

    public async deleteImage(request, response) {
        const exists = promisify(fs.exists);
        const unlink = promisify(fs.unlink);
        if (request.item.image) {
            const path = `public/images/${request.item.image}`;
            const fileExist = await exists(path);
            if (fileExist) {
                await unlink(path);
            }
        }
        await db.items.update({ image: null }, { where: { id: request.item.id } });

        response.sendStatus(200);
    }
}

export default new ItemController();
