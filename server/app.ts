import * as path from 'path';
import * as express from 'express';
import * as logger from 'morgan';
import * as bodyParser from 'body-parser';
import * as compression from 'compression';
import * as helmet from 'helmet';
import * as cors from 'cors';
import api from './routes/api';

class App {
    public express: express.Application;

    constructor() {
        this.express = express();
        this.middleware();
        this.routes();
    }

    private middleware(): void {
        this.express.use(logger('dev'));
        this.express.use(bodyParser.json());
        this.express.use(bodyParser.urlencoded({ extended: false }));
        this.express.use(express.static('public'));
        this.express.use(compression());
        this.express.use(helmet());
        this.express.use(cors({credentials: true, origin: 'http://localhost:3000'}))
    }

    private routes(): void {
        this.express.use('/api/', api);
    }
}

export default new App().express;
