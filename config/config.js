const env = process.env.NODE_ENV || 'noEnv';

const development = {
    dbConfig: {
        host: '127.0.0.1',
        port: 3306,
        database: 'publications',
        username: 'root',
        password: 'root',
        socketPath: '/Applications/MAMP/tmp/mysql/mysql.sock'
    },
    jwt: {
        secret: 'yo-its-a-secret',
        expireTime: '1d'
    },

};

const noEnv = {
    dbConfig: {
        host: '127.0.0.1',
        port: 3306,
        database: 'publications',
        username: 'root',
        password: 'root',
        socketPath: '/Applications/MAMP/tmp/mysql/mysql.sock'
    },
    jwt: {
        secret: 'yo-its-a-secret',
        expireTime: '1d'
    }
};

const config = {
    development,
    noEnv
};

module.exports = config[env];
